//consignment-service/main/go
package main

import (

	"fmt"
	"log"
	"errors"

	"os"

	// Import the generated protobuf code
	pb "gitlab.com/schnjess/consignment-service/proto/consignment"
	vesselProto "gitlab.com/schnjess/vessel-service/proto/vessel"
	userService "gitlab.com/schnjess/user-service/proto/auth"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/server"

	"golang.org/x/net/context"


)

const (
	defaultHost = "localhost:27017"
)


func main() {

	// Database host from environment variables
	host := os.Getenv("DB_HOST")
	log.Println("DB_HOST: ", host)

	if host == "" {
		host = defaultHost
	}
	log.Println("DB_HOST: ", host)

	session, err := CreateSession(host)

	// Mgo creates a 'master' session, we need to end that session
	// before the main function closes.
	defer session.Close()

	if err != nil {

		// We're wrapping the error returned from our CreateSession
		// here to add some context to the error.
		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}


	//Create a new service. Optionally include some options here
	srv := micro.NewService(

		//This name must match the package name given in the protobuf definition
		micro.Name("project.consignment"),
		micro.Version("latest"),
		// our auth middleware
		micro.WrapHandler(AuthWrapper),
	)


	vesselClient := vesselProto.NewVesselService("go.micro.srv.vessel", srv.Client())

	// Init will parse the command line flags
	srv.Init()

	// Register handler
	pb.RegisterConsignmentServiceHandler(srv.Server(), &service{session, vesselClient})

//run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}

func AuthWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, resp interface{}) error {
		// this skips our auth check if DISABLE_AUTH is set to true
		if os.Getenv("DISABLE_AUTH") == "true" {
			return fn(ctx, req,resp)
		}

		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return errors.New("no auth meta-data found in request")
		}
		token := meta["Token"]
		log.Println("Authenticating with token: ", token)

		//auth here
		authClient := userService.NewAuthService("auth", client.DefaultClient)
		authResp, err := authClient.ValidateToken(ctx, &userService.Token {
			Token: token,
		})
		log.Println("Auth resp:", authResp)
		log.Println("Err:", err)
		if err != nil {
			return err
		}

		err = fn(ctx, req, resp)
		return err
	}
}
