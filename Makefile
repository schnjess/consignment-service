build:
	protoc --plugin=protoc-gen-go=${GOPATH}/bin/protoc-gen-go \
	--plugin=protoc-gen-micro=${GOPATH}/bin/protoc-gen-micro \
	--proto_path=${GOPATH}/src:. --micro_out=. \
	--go_out=. proto/consignment/consignment.proto
	GOOS=linux GOARCH=amd64
	docker build -t consignment-service .
run:
	docker run --net="host" \
	-p 50052 \
	-e MICRO_SERVER_ADDRESS=:50052 \
	-e MICRO_REGISTRY=mdns  \
	-e DISABLE_AUTH=true \
	consignment-service
