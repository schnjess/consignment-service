# consignment-service/Dockerfile

# We use the offical golang image, which contains all the
# correct build tools and libraries. Notice `as builder`,
# this gives this container a name that we can reference later on.

FROM golang:1.11.0 as builder

# set our workdir to our current service in the GOPATH
WORKDIR /go/src/consignment-service

# copy the current code into our workdir
COPY . .

RUN go get

# build the binary, with a few flags which will allow
# us to run this binary in Alpine/debian
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .

# Here we're using a second FROM statement, which is strange,
# but this tells docker to start a new build process with this image
FROM debian:latest

#same as before, create directory for app
RUN mkdir /app
WORKDIR /app

# here, instead of copying the binary from our host machine,
#we pull the binary from hte container named `builder`, within,
# this build context. THis reaches into our previous image, finds
# the binary we built, and pulls in into this container. Amazing!
COPY --from=builder /go/src/consignment-service/consignment-service .

# run the binary as per usual! This time with a binary build in a
#separate container, with all of the correct dependencies and
# run time libraries.
CMD ["./consignment-service"]
